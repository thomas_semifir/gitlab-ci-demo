package demo;

import java.util.ArrayList;
import java.util.List;

public class Pyramide {

    /**
     * Cette méthode retourne une liste de String représentant une  pyramide d'étoiles.
     * <p>
     *     pyramide(3) : [*, **, ***, **, *]
     * </p>
     *
     * Elle droit retourner une liste de String
     * Elle doit avoir une certaine taille (pour une taille de 3 elle doit avoir une longueur de 5 String)
     * Ses éléments doivent être composés uniquement d'étoiles
     *
     *
     * @param taille de l'élément le plus grand de la pyramide
     * @return la liste contenant les étages de la pyramide
     */
    public List<String> pyramide(Integer taille) throws PyramideBadArgumentException{
        verificationTailleValide(taille);
        List<String> pyramide = new ArrayList<>();
        creationFaceCroissante(taille, pyramide);
        creationFaceDecroissante(taille, pyramide);
        return pyramide;
    }

    private void creationFaceDecroissante(Integer taille, List<String> pyramide) {
        for(int i = taille -1; i>0; i--){
            pyramide.add("*".repeat(i));
        }
    }

    private void creationFaceCroissante(Integer taille, List<String> pyramide) {
        for(int i = 0; i< taille; i++){
            pyramide.add("*".repeat(i+1));
        }
    }

    private void verificationTailleValide(Integer taille) {
        if(taille < 1) {
            throw new PyramideBadArgumentException("la taille de la pyramide doit être supérieure ou égale à 1, ici elle est de " + taille);
        }
    }
}
